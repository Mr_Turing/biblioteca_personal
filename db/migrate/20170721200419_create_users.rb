class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :nombres
      t.string :apellidos
      t.string :cedula
      t.string :email
      t.timestamps
    end
  end
end
