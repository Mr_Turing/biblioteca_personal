class CreateBooks < ActiveRecord::Migration[5.1]
  def change
    create_table :books do |t|
      t.string :nombre
      t.string :autor
      t.string :descripcion
      # t.attachment :image
      t.references :user # Referencia a la llave foranea de la tabla User
      t.timestamps
    end
  end
end
