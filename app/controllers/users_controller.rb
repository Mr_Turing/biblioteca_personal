class UsersController < ApplicationController
  # aqui vemos todos los usuarios
  def index
    @users = User.all
  end
  # Aqui ingresamos los registros de los nuevos libros
  def new
    @user = User.new
  end

  # Aqui vemos cada usuario por individual
  def show
    @user = User.find(params[:id])
  end

  # Aqui editamos cada uno de los usuarios
  def edit
    @user = User.find(params[:id])
  end

  # Aqui actualizamos cada uno de los libros editados
  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      redirect_to(:action => 'index', :id => @user.id)
    else
      render 'edit'
    end
  end

  # Aqui creamos un nuevo usuario
  def create
    @user = User.new(user_params)

    if @user.save
      redirect_to '/users'
    else
      flash.now[:alert] = @user.errors[:email].first
      render 'new'
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy

    redirect_to '/users'

  end

  # tomamos los objetos del metodo libro los cuales vamos a editar
  private
  def user_params
    params.require(:user).permit(:nombres, :apellidos ,:cedula, :email)
  end



end
