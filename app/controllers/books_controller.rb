class BooksController < ApplicationController

  def array_user
    @users = User.all
    @users_id_name2 = Array.new()
    @users.each do |u|
      arraT = Array.new()
      @users_id_name2.push(arraT.push(u.nombres.capitalize! + " "+ u.apellidos.capitalize!, u.id))
    end

    return @users_id_name2
  end


  # Aqui ingresamos los registros de los nuevos libros
  def new
    @book = Book.new
    @users_id_name  = array_user
  end

  # Aqui vemos todos los libros
  def index
    @books = Book.all
  end

  # Aqui vemos cada libro por individual
  def show
    @book = Book.find(params[:id])
  end

  def prestar
    @book = Book.find(params[:id])
    @users_id_name  = array_user
  end

  def prestamos
    @books = Book.all
  end

  # Aqui editamos cada uno de los libros
  def edit
    @book = Book.find(params[:id])
    @users_id_name  = array_user
  end

  # Aqui actualizamos cada uno de los libros editados
  def update
    @book = Book.find(params[:id])
    if @book.update_attributes(book_params)
      redirect_to(:action => 'index', :id => @book.id)
    else
      render 'edit'
    end
  end

  # Aqui creamos un nuevo libro
  def create
    @book = Book.new(book_new_params)
    if @book.save
      redirect_to '/books'
    else
      render 'new'
    end
  end

  def destroy
    @book = Book.find(params[:id])
    @book.destroy

    redirect_to '/books'

  end


  private
  def book_new_params
    params.require(:book).permit(:nombre, :autor, :descripcion, :image, :user_id)
  end

  # tomamos los objetos del metodo libro los cuales vamos a editar
  private
  def book_params
    params.require(:book).permit(:nombre, :autor ,:descripcion,:user_id)
  end



end
