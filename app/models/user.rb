class User < ApplicationRecord
  # Un usuario tiene multiples libros
  has_many :books

  validates :nombres, :apellidos, :cedula, :presence => true

  validates :email,
            :presence => true,
            :uniqueness =>  {:message => "Esta cuenta de correo ya se encuentra registrada!"},
            :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }
end
