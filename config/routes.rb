Rails.application.routes.draw do

  # Definimos la ruta para ingresar un nuevo libro
  get '/books/new' => 'books#new'
  post 'books' => 'books#create'

  # Definimos la ruta para ver el listado de los prestamos
  get '/books/prestamos' => 'books#prestamos'


  # Definimos la ruta para ingresar un nuevo usuario
  get '/users/new' => 'users#new'
  post 'users' => 'users#create'

  # Definimos la ruta para editar y actualizar la informacion de los libros
  get '/books/:id/prestar' => 'books#prestar', as: :prestar_book
  patch '/books/:id'  => 'books#update'

  # Definimos la ruta para eliminar un usuario
  delete 'users/:id' => 'users#destroy', as: :destroy_user

  delete 'books/:id' => 'books#destroy', as: :destroy_book

  # Definimos la ruta para ver todos los usuarios
  get '/users' => 'users#index'

  # Definimos la ruta para ver todos los libros
  get '/books' => 'books#index'

  # Definimos la ruta para ver un libro en especifico
  get '/books/:id' => 'books#show', as: :book

  # Definimos la ruta para editar y actualizar la informacion de los libros
  get '/books/:id/edit' => 'books#edit', as: :edit_book
  patch '/books/:id' => 'books#update'

  # Definimos la ruta para ver un usuario en especifico
  get '/users/:id' => 'users#show', as: :user

  # Definimos la ruta para editar y actualizar la informacion de un usuario
  get '/users/:id/edit' => 'users#edit', as: :edit_user
  patch '/users/:id' => 'users#update'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
